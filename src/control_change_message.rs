#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum ControlChangeMessage {
    SoundController1, // 0 : A control for affecting how the sound is produced. Used for filters, effects etc.
    ModulationWheel_MSB, // 1 : This controls modulation for live performances. It controls the parameter(s) it's mapped to in your synth, instrument or effect. Commonly used for filter cutoff or vibrato.
    BreathController_MSB, // 2 : Originally for use with an electronic breath MIDI controller that could read changes in pressure of breath. Can be used for modulation in performances, and is often used with aftertouch.
    FootPedal_MSB, // 4 : Used to send messages with an expression pedal. Mostly used for live performance modulation, and commonly used for aftertouch messages.
    PortamentoTime_MSB, // 5 : Changes the rate of glide between 2 different notes. This causes a pitch bend between 2 notes when it's on a value.
    DataEntry_MSB,      // 6 : Controls SYSEX, NRPN and RPN values.
    Volume,             // 7 : Changes the volume of your VST instrument patch.
    Balance_MSB, // 8 : Controls LR (left, right) balance for instruments with a stereo signal. 0 = left, 64 = centre, 127 = right.
    Legato,      // 8 : A switch to turn the legato on and off.
    Pan_MSB, // 10 : This controls panning for mono instruments. 0 = left, 64 = centre, 127 = right. Can be used for stereo instruments too. CC8 is often used for panning instead.
    ExpressionPedal_MSB, // 11 : Pedal used for live performance modulation. Map to parameters inside your instrument to modulate while playing.
    EffectController1_MSB, // 12 : For controlling effects in an instrument.
    EffectController2_MSB, // 13 : For controlling effects in an instrument.
    SliderKnobOrRibbonControllerForGeneralPurpose1, // 16-19 : A general-purpose MIDI controller for performance modulation.
    SliderKnobOrRibbonControllerForGeneralPurpose2, // 16-19 : A general-purpose MIDI controller for performance modulation.
    SliderKnobOrRibbonControllerForGeneralPurpose3, // 16-19 : A general-purpose MIDI controller for performance modulation.
    SliderKnobOrRibbonControllerForGeneralPurpose4, // 16-19 : A general-purpose MIDI controller for performance modulation.
    BankSelect_LSB, // 32 : Used alongside CC0 bank selection. Changes to a fresh bank of patches if possible.
    ModulationWheel_LSB, // 33 : Used alongside CC1 to send a modulation command for instruments with higher mod resolution.
    BreathController_LSB, // 34 : Used alongside CC2 to send a modulation command for instruments with higher mod resolution.
    FootPedal_LSB, // 36 : Used alongside CC4 to send a modulation command for instruments with higher mod resolution.
    PortamentoTime_LSB, // 37 : Used alongside CC5 to send a modulation command for instruments with higher mod resolution.
    DataEntry_LSB,      // 38 : Used alongside CC6 for SYSEX, NRPN, or RPN messages.
    Volume_LSB, // 39 : Used alongside CC7 for instruments with higher volume modulation resolution.
    Balance_LSB, // 40 : Used with CC8 to send a modulation command for instruments with greater modulation resolution.
    Pan_LSB, // 42 : Used with CC10 to send modulation commands for instruments with higher mod resolution.
    Expression_LSB, // 43 : Used with CC11 to send modulation commands for instruments with higher mod resolution.
    EffectControl1_LSB, // 44 : Used with CC12 to send modulation commands for instruments with higher mod resolution.
    EffectControl2_LSB, // 45 : Used with CC13 to send modulation commands for instruments with higher mod resolution.
    SustainPedal, // 64 : One of the most commonly used MIDI channels. Controls the sustain on an instrument. Almost all instruments will use this command.
    Portamento,   // 65 : A switch for Portamento time used to turn it on and off.
    Sostenuto,    // 66 : A switch for sustain of active notes is used to turn sustain on and off.
    SoftPedal, // 67 : A switch to turn the soft pedal on and off. Supposed to emulate the Piano soft pedal.
    HoldPedal2, // 69 : An alternative control to sustain that affects how notes are held and fade out.
    SoundController2, // 71 : Allocated to filter resonance/Q.
    SoundController3, // 72 : Allocated to the amp envelope release time. Changes how long notes fade out.
    SoundController4, // 73 : Allocated to the amp envelope attack time. Changes how fast the volume rises from the keypress to max volume.
    SoundController5, // 74 : Allocated to the filter cutoff frequency Hz value.
    SoundController6, // 75 – 79 : An extra control for affecting how the sound is produced. Used for filters, effects etc.
    SoundController7, // 75 – 79 : An extra control for affecting how the sound is produced. Used for filters, effects etc.
    SoundController8, // 75 – 79 : An extra control for affecting how the sound is produced. Used for filters, effects etc.
    SoundController9, // 75 – 79 : An extra control for affecting how the sound is produced. Used for filters, effects etc.
    SoundController10, // 75 – 79 : An extra control for affecting how the sound is produced. Used for filters, effects etc.
    GeneralPurpose1,   // 80 – 84 : An on off switch for general purpose.
    GeneralPurpose2,   // 80 – 84 : An on off switch for general purpose.
    GeneralPurpose3,   // 80 – 84 : An on off switch for general purpose.
    GeneralPurpose4,   // 80 – 84 : An on off switch for general purpose.
    GeneralPurpose5,   // 80 – 84 : An on off switch for general purpose.
    Effect1Depth,      // 91 : Usually a control for reverb in your instrument.
    Effect2Depth,      // 92 : Usually a control for the amount of tremolo.
    Effect3Depth,      // 93 : Usually a control for the amount of chorus.
    Effect4Depth,      // 94 : Usually a control for the amount of detuning.
    Effect5Depth,      // 95 : Usually a control for the amount of phasing.
    DataBoundIncrementLSB, // 96 : A control to increment data for SYSEX, NRPN, RPN.
    DataBoundIncrementMSB, // 97 : A control to decrement data for SYSEX, NRPN, RPN.
    NRPN_LSB,          // 98 : Selects the NRPN variable for CC6, 38, 96 & 97.
    NRPN_MSB,          // 99 : Selects the NRPN variable for CC6, 38, 96 & 97.
    RPN_LSB,           // 100 : Selects the RPN variable for CC6, 38, 96 & 97.
    RPN_MSB,           // 101 : Selects the RPN variable for CC6, 38, 96 & 97.
    ChannelMute, // 120 : Turns off all sound immediately, paying no attention to release or sustain.
    ResetAllControllers, // 121 : Resets all controllers to default.
    LocalKeyboard, // 122 : A switch for local keyboard mode, to turn on and off. Used to turn off the internal sound.
    AllNotes, // 123 : A switch to turn all notes on, or off. Sustain and release will be maintained.
    OMNIModeOFF, // 124 : A switch to turn OMNI Mode off. OMNI on will send & receive information on all MIDI channels, rather than a particular one.
    OMNIModeON, // 125 : A switch to turn OMNI Mode off. OMNI on will send & receive information on all MIDI channels, rather than a particular one.
    MonoMode,   // 126 : Tells an instrument to work in Mono. Turns off Poly.
    PolyMode,   // 127 : Tells an instrument to work in Poly mode. Turns off Mono.
    Undefined,
}

impl ControlChangeMessage {
    pub fn from_byte(byte: &u8) -> Self {
        match byte {
            // Descriptions taken from: https://www.whippedcreamsounds.com/midi-cc-list/
            0 => Self::SoundController1, // 0 : A control for affecting how the sound is produced. Used for filters, effects etc.
            1 => Self::ModulationWheel_MSB, // 1 : This controls modulation for live performances. It controls the parameter(s) it's mapped to in your synth, instrument or effect. Commonly used for filter cutoff or vibrato.
            2 => Self::BreathController_MSB, // 2 : Originally for use with an electronic breath MIDI controller that could read changes in pressure of breath. Can be used for modulation in performances, and is often used with aftertouch.
            4 => Self::FootPedal_MSB, // 4 : Used to send messages with an expression pedal. Mostly used for live performance modulation, and commonly used for aftertouch messages.
            5 => Self::PortamentoTime_MSB, // 5 : Changes the rate of glide between 2 different notes. This causes a pitch bend between 2 notes when it's on a value.
            6 => Self::DataEntry_MSB,      // 6 : Controls SYSEX, NRPN and RPN values.
            7 => Self::Volume,             // 7 : Changes the volume of your VST instrument patch.
            8 => Self::Balance_MSB, // 8 : Controls LR (left, right) balance for instruments with a stereo signal. 0 = left, 64 = centre, 127 = right.
            9 => Self::Legato,      // 9 : A switch to turn the legato on and off.
            10 => Self::Pan_MSB, // 10 : This controls panning for mono instruments. 0 = left, 64 = centre, 127 = right. Can be used for stereo instruments too. CC8 is often used for panning instead.
            11 => Self::ExpressionPedal_MSB, // 11 : Pedal used for live performance modulation. Map to parameters inside your instrument to modulate while playing.
            12 => Self::EffectController1_MSB, // 12 : For controlling effects in an instrument.
            13 => Self::EffectController2_MSB, // 13 : For controlling effects in an instrument.
            16 => Self::SliderKnobOrRibbonControllerForGeneralPurpose1, // 16-19 : A general-purpose MIDI controller for performance modulation.
            17 => Self::SliderKnobOrRibbonControllerForGeneralPurpose2, // 16-19 : A general-purpose MIDI controller for performance modulation.
            18 => Self::SliderKnobOrRibbonControllerForGeneralPurpose3, // 16-19 : A general-purpose MIDI controller for performance modulation.
            19 => Self::SliderKnobOrRibbonControllerForGeneralPurpose4, // 16-19 : A general-purpose MIDI controller for performance modulation.
            32 => Self::BankSelect_LSB, // 32 : Used alongside CC0 bank selection. Changes to a fresh bank of patches if possible.
            33 => Self::ModulationWheel_LSB, // 33 : Used alongside CC1 to send a modulation command for instruments with higher mod resolution.
            34 => Self::BreathController_LSB, // 34 : Used alongside CC2 to send a modulation command for instruments with higher mod resolution.
            36 => Self::FootPedal_LSB, // 36 : Used alongside CC4 to send a modulation command for instruments with higher mod resolution.
            37 => Self::PortamentoTime_LSB, // 37 : Used alongside CC5 to send a modulation command for instruments with higher mod resolution.
            38 => Self::DataEntry_LSB, // 38 : Used alongside CC6 for SYSEX, NRPN, or RPN messages.
            39 => Self::Volume_LSB, // 39 : Used alongside CC7 for instruments with higher volume modulation resolution.
            40 => Self::Balance_LSB, // 40 : Used with CC8 to send a modulation command for instruments with greater modulation resolution.
            42 => Self::Pan_LSB, // 42 : Used with CC10 to send modulation commands for instruments with higher mod resolution.
            43 => Self::Expression_LSB, // 43 : Used with CC11 to send modulation commands for instruments with higher mod resolution.
            44 => Self::EffectControl1_LSB, // 44 : Used with CC12 to send modulation commands for instruments with higher mod resolution.
            45 => Self::EffectControl2_LSB, // 45 : Used with CC13 to send modulation commands for instruments with higher mod resolution.
            64 => Self::SustainPedal, // 64 : One of the most commonly used MIDI channels. Controls the sustain on an instrument. Almost all instruments will use this command.
            65 => Self::Portamento, // 65 : A switch for Portamento time used to turn it on and off.
            66 => Self::Sostenuto, // 66 : A switch for sustain of active notes is used to turn sustain on and off.
            67 => Self::SoftPedal, // 67 : A switch to turn the soft pedal on and off. Supposed to emulate the Piano soft pedal.
            69 => Self::HoldPedal2, // 69 : An alternative control to sustain that affects how notes are held and fade out.
            71 => Self::SoundController2, // 71 : Allocated to filter resonance/Q.
            72 => Self::SoundController3, // 72 : Allocated to the amp envelope release time. Changes how long notes fade out.
            73 => Self::SoundController4, // 73 : Allocated to the amp envelope attack time. Changes how fast the volume rises from the keypress to max volume.
            74 => Self::SoundController5, // 74 : Allocated to the filter cutoff frequency Hz value.
            75 => Self::SoundController6, // 75 – 79 : An extra control for affecting how the sound is produced. Used for filters, effects etc.
            76 => Self::SoundController7, // 75 – 79 : An extra control for affecting how the sound is produced. Used for filters, effects etc.
            77 => Self::SoundController8, // 75 – 79 : An extra control for affecting how the sound is produced. Used for filters, effects etc.
            78 => Self::SoundController9, // 75 – 79 : An extra control for affecting how the sound is produced. Used for filters, effects etc.
            79 => Self::SoundController10, // 75 – 79 : An extra control for affecting how the sound is produced. Used for filters, effects etc.
            80 => Self::GeneralPurpose1,   // 80 – 84 : An on off switch for general purpose.
            81 => Self::GeneralPurpose2,   // 80 – 84 : An on off switch for general purpose.
            82 => Self::GeneralPurpose3,   // 80 – 84 : An on off switch for general purpose.
            83 => Self::GeneralPurpose4,   // 80 – 84 : An on off switch for general purpose.
            84 => Self::GeneralPurpose5,   // 80 – 84 : An on off switch for general purpose.
            91 => Self::Effect1Depth,      // 91 : Usually a control for reverb in your instrument.
            92 => Self::Effect2Depth,      // 92 : Usually a control for the amount of tremolo.
            93 => Self::Effect3Depth,      // 93 : Usually a control for the amount of chorus.
            94 => Self::Effect4Depth,      // 94 : Usually a control for the amount of detuning.
            95 => Self::Effect5Depth,      // 95 : Usually a control for the amount of phasing.
            96 => Self::DataBoundIncrementLSB, // 96 : A control to increment data for SYSEX, NRPN, RPN.
            97 => Self::DataBoundIncrementMSB, // 97 : A control to decrement data for SYSEX, NRPN, RPN.
            98 => Self::NRPN_LSB, // 98 : Selects the NRPN variable for CC6, 38, 96 & 97.
            99 => Self::NRPN_MSB, // 99 : Selects the NRPN variable for CC6, 38, 96 & 97.
            100 => Self::RPN_LSB, // 100 : Selects the RPN variable for CC6, 38, 96 & 97.
            101 => Self::RPN_MSB, // 101 : Selects the RPN variable for CC6, 38, 96 & 97.
            120 => Self::ChannelMute, // 120 : Turns off all sound immediately, paying no attention to release or sustain.
            121 => Self::ResetAllControllers, // 121 : Resets all controllers to default.
            122 => Self::LocalKeyboard, // 122 : A switch for local keyboard mode, to turn on and off. Used to turn off the internal sound.
            123 => Self::AllNotes, // 123 : A switch to turn all notes on, or off. Sustain and release will be maintained.
            124 => Self::OMNIModeOFF, // 124 : A switch to turn OMNI Mode off. OMNI on will send & receive information on all MIDI channels, rather than a particular one.
            125 => Self::OMNIModeON, // 125 : A switch to turn OMNI Mode off. OMNI on will send & receive information on all MIDI channels, rather than a particular one.
            126 => Self::MonoMode,   // 126 : Tells an instrument to work in Mono. Turns off Poly.
            127 => Self::PolyMode, // 127 : Tells an instrument to work in Poly mode. Turns off Mono.
            _ => Self::Undefined,
        }
    }
}
