#![no_std]
#![allow(unused)]
#![allow(non_camel_case_types)]

// MidiMessage enum and implementations
mod midi_message;
pub use crate::midi_message::*;

// ControlChangeMessage and implementations
mod control_change_message;
pub use crate::control_change_message::*;

// ProgramChangeMessage and implementations
mod program_change_message;
pub use crate::program_change_message::*;

// Helpers for getting note frequencies
mod frequencies;
pub use crate::frequencies::*;

// MidiNote enum and helpers
mod notes;
pub use crate::notes::*;

// #####################################################################
// Tests
// #####################################################################

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_bytes_conversion() {
        let raw_message: [u8; 3] = [0b1000_0011, 69, 69];
        assert_eq!(
            MidiMessage::from_bytes(&raw_message),
            MidiMessage::NoteOff {
                channel: 3,
                note: 69,
                vel: 69,
                bytes: [3, 69, 69],
            }
        );
    }
}
