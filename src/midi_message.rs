use crate::control_change_message::ControlChangeMessage;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum MidiMessage {
    NoteOn {
        channel: u8,
        note: u8,
        vel: u8,
        bytes: [u8; 3],
    }, // Note on message.
    NoteOff {
        channel: u8,
        note: u8,
        vel: u8,
        bytes: [u8; 3],
    }, // Note off message
    PolyphonicKeyPressure {
        channel: u8,
        note: u8,
        vel: u8,
        bytes: [u8; 3],
    }, // Polyphonic aftertouch
    ChannelPressure {
        channel: u8,
        vel: u8,
        bytes: [u8; 3],
    }, // Monophonic aftertouch
    ControlChange {
        channel: u8,
        message_type: ControlChangeMessage,
        value: u8,
        bytes: [u8; 3],
    },
    ProgramChange {
        channel: u8,
        patch_number: u8,
        bytes: [u8; 3],
    },
    PitchBend {
        channel: u8,
        bend: u16,
        bytes: [u8; 3],
    }, // 14bit value 0lllllll 0mmmmmmm
    ChannelMode {
        channel: u8,
        controller: u8,
        value: u8,
        bytes: [u8; 3],
    },
    SystemCommon {
        channel: u8,
        byte1: u8,
        byte2: u8,
        bytes: [u8; 3],
    },
    Undefined {
        bytes: [u8; 3],
    },
}

impl MidiMessage {
    // Convert raw MIDI bytes to MidiMessage
    // TODO:
    // - Check if the LSB and MSB are not switched.
    // - Maybe implement detection of MSB and LSB together? Probably not a good idea.
    // - Check for missing messages.
    pub fn from_bytes(bytes: &[u8]) -> Self {
        let channel = bytes[0] & 0b0000_1111;
        match bytes[0] & 0b1111_0000 {
            0b1000_0000 => Self::NoteOff {
                channel,
                note: bytes[1],
                vel: bytes[2],
                bytes: [bytes[0], bytes[1], bytes[2]],
            },
            0b1001_0000 => Self::NoteOn {
                channel,
                note: bytes[1],
                vel: bytes[2],
                bytes: [bytes[0], bytes[1], bytes[2]],
            },
            0b1010_0000 => Self::PolyphonicKeyPressure {
                channel,
                note: bytes[1],
                vel: bytes[2],
                bytes: [bytes[0], bytes[1], bytes[2]],
            },
            0b1100_0000 => Self::ProgramChange {
                channel,
                patch_number: bytes[1],
                bytes: [bytes[0], bytes[1], bytes[2]],
            },
            0b1101_0000 => Self::ChannelPressure {
                channel,
                vel: bytes[2],
                bytes: [bytes[0], bytes[1], bytes[2]],
            },
            0b1111_0000 => Self::SystemCommon {
                channel,
                byte1: bytes[1],
                byte2: bytes[2],
                bytes: [bytes[0], bytes[1], bytes[2]],
            },
            0b1110_0000 => Self::PitchBend {
                channel,
                bend: (bytes[1] as u16) | ((bytes[2] as u16) << 7),
                bytes: [bytes[0], bytes[1], bytes[2]],
            },
            0b1011_0000 => Self::ControlChange {
                channel,
                message_type: ControlChangeMessage::from_byte(&bytes[1]),
                value: bytes[2],
                bytes: [bytes[0], bytes[1], bytes[2]],
            },
            _ => Self::Undefined {
                bytes: [bytes[0], bytes[1], bytes[2]],
            },
        }
    }
}
