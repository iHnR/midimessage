# MIDI Message
This is a little utility containing some useful abstractions for midi messages. This is just for personal use and I might change stuff later on.

# Types
The two main types in this crate for now are:
## MidiMessage
An enum that represents the main midi messages.

## ControlChangeMessage
An enum representing the different types of control change messages. I'm think of changins this to more directly support messages with an LSB/MSB type.
